<#import "/spring.ftl" as spring>

<html>
    <head>
    <link rel="stylesheet" href="css/styles.css" type="text/css"/>
    </head>
    <body>
        <h1>Welcome to Tech Brothers - Tech Solutions</h1>
        <h3>User Data </h3>     
        <table border="1" align="center">
            <tr>
                <th>Name</th>
                <th>Age</th>
                <th>Email Id</th>
            </tr>
            <tr>
                <td>${user.name}</td>
                <td>${user.age}</td>
                <td>${user.emailId}</td>
            </tr>
         </table>   
        
    </body>
</html>

